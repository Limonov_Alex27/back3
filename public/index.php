<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    echo 'Спасибо, результаты сохранены.';
  }
  include('index.html');
  exit();
}

$name = $_POST['field-name'];
$email = $_POST['field-email'];
$date = $_POST['field-date'];
$radio1 = $_POST['radio-group-1'];
$radio2 = $_POST['radio-group-2'];
$superpower = $_POST['superpower'];
$bio = $_POST['bio'];
$check = $_POST['check-1'];


$errors = FALSE;
if (empty($_POST['field-name'])) {
  echo "<script type='text/javascript'>alert('Заполните имя.');</script>";
  $errors = TRUE;
}


if ($email == '') {
  echo "<script type='text/javascript'>alert('Заполните e-mail.');</script>";
  $errors = TRUE;
}


if (empty($_POST['field-date'])) {
  echo "<script type='text/javascript'>alert('Заполните дату рождения.');</script>";
  $errors = TRUE;
}


if (empty($_POST['radio-group-1'])) {
  echo "<script type='text/javascript'>alert('Заполните данные.');</script>";
  $errors = TRUE;
}


if (empty($_POST['radio-group-2'])) {
  echo "<script type='text/javascript'>alert('Заполните данные.');</script>";
  $errors = TRUE;
}


if (empty($_POST['bio'])) {
  echo "<script type='text/javascript'>alert('Заполните биографию.');</script>";
  $errors = TRUE;
}


if (empty($_POST['check-1'])) {
  echo "<script type='text/javascript'>alert('Приминте условия.');</script>";
  $errors = TRUE;
}

if ($errors) {
  exit();
}

$user = 'u24214';
$pass = '3455645';
$db = new PDO('mysql:host=localhost;dbname=u24214', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $stmt = $db->prepare("INSERT INTO form27 (name,email,date,radio1,radio2,superpower,bio,check1) VALUE (:name,:email,:date,:radio1,:radio2,:superpower,:bio,:check1)");
  $stmt -> execute(['name'=>$name,'email'=>$email,'date'=>$date,'radio1'=>$radio1,'radio2'=>$radio2,'superpower'=>$superpower,'bio'=>$bio,'check1'=>$check]);
  echo "<script type='text/javascript'>alert('Спасибо, результаты сохранены.');</script>";
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

